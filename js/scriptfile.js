const firstTemplate = document.getElementsByClassName("template")[0].cloneNode(true);

const table = document.getElementById("table");

var listOfTemplates = [];

let template = new Template(document.getElementsByClassName("template")[0]);
listOfTemplates.push(template);


function deleteTemplate() {
    deleteTemplates(true);
}

function addTemplate() {
    generateTemplate();
}

function saveTemplate() {
    if (!calculate()) {
        alert("You can't save your template with calculation errors.");
        return;
    }
    let text = "";
    for (let i in listOfTemplates)
        text += listOfTemplates[i].name.value + ";" + listOfTemplates[i].damage.value + ";" + listOfTemplates[i].unitsAmount.value + ";" + listOfTemplates[i].consumptionModifier.value + "/";
    GenerateDownloadLink(text);
}

//#region Loading

function loadTemplate(text) {
    deleteTemplates();
    let breaks = text.split('/');

    /* POP TO REMOVE BLANK LINE */
    breaks.pop();

    for (let i = 0; i < breaks.length; i++) {
        let param = breaks[i].split(';');
        generateTemplate(param[0], param[1], param[2], param[3]);
    }
}

function openFileMenu() {
    loadFile.click();
}

//#endregion Loading

let totalNeededSupply = document.getElementById("total-needed-supply");
let totalDamage = document.getElementById("total-damage-all");
/* calculate returns false if it gets any error */
function calculate() {
    let sum = [0, 0];
    for (let i = 0; i < listOfTemplates.length; i++) {
        
        let amt = listOfTemplates[i].calculate();

        if (amt == false || listOfTemplates[i].name.value.includes(";")) return false;

        for (let i = 0; i < sum.length; i++) sum[i] += amt[i];
    }
    
    totalDamage.innerHTML = "Total Damage: " + sum[0];
    totalNeededSupply.innerHTML = "Total Needed Supply: " + sum[1];
    return true;
}