class Template {

    constructor(node, name="Name", damage=0, unitsAmount=0, consumptionModifier=.1) {
        this.node = node;

        //#region Inputs Div
        this.inputsDiv = this.node.getElementsByClassName("inputs")[0];
        this.checkBox = this.inputsDiv.getElementsByClassName("template-check-box")[0];
        this.name = this.inputsDiv.getElementsByClassName("name")[0];
        this.damage = this.inputsDiv.getElementsByClassName("damage")[0];
        this.unitsAmount = this.inputsDiv.getElementsByClassName("units-amount")[0];
        this.consumptionModifier = this.inputsDiv.getElementsByClassName("consumption-modifier")[0];
        //#endregion

        this.name.value = name;
        this.damage.value = damage;
        this.unitsAmount.value = unitsAmount;
        this.consumptionModifier.value = consumptionModifier;



        //#region Info Div
        this.infoDiv = this.node.getElementsByClassName("info")[0];
        this.neededSupply = this.infoDiv.getElementsByClassName("needed-supply")[0];
        this.totalDamage = this.infoDiv.getElementsByClassName("total-damage")[0];
        //#endregion

    }

    isChecked() {
        return this.checkBox.checked;
    }

    calculate() {
        try {
            this.name.value = this.name.value.split(';').join("");
            let unitsAmount = parseInt(this.unitsAmount.value);
            let damage = parseFloat(this.damage.value);
            let supplyModifier = parseFloat(this.consumptionModifier.value);

            let totalDamage = unitsAmount * damage;
            let supplyConsumption = totalDamage * supplyModifier;

            this.totalDamage.innerHTML = "Total Damage: " + totalDamage.toFixed(2);
            this.neededSupply.innerHTML = "Supply Consumption: " + supplyConsumption.toFixed(2);

            return [totalDamage, supplyConsumption];
        } catch(e) {
            alert("Error in unit.\nName: " + e);
            return false;
        }
    }
}

function generateTemplate() {
    let newTemplate = firstTemplate.cloneNode(true);
    table.append(newTemplate);
    listOfTemplates.push(new Template(newTemplate));
}

function generateTemplate(name, damage, unitsAmount, supplyConsumption) {
    let newTemplate = firstTemplate.cloneNode(true);
    table.append(newTemplate);
    listOfTemplates.push(new Template(newTemplate, name, damage, unitsAmount, supplyConsumption));
}

function deleteTemplates(onlySelecteds=false) {
    let removed = 0;
    for (let i = 0; i - removed < listOfTemplates.length; i++) {
        if (!onlySelecteds || listOfTemplates[i - removed].isChecked()) {
            listOfTemplates[i - removed].node.remove();
            listOfTemplates.splice(i - removed, 1);
            removed++;
        }
    }
}