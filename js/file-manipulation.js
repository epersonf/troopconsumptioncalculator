

let btn = document.getElementById('download-button');

function GenerateDownloadLink(text) {
    var data = new Blob([text], {type: 'text/plain'});

    var url = window.URL.createObjectURL(data);
    
    btn.href = url;
    btn.download = "TemplateInfo.txt";
    btn.click();
    window.URL.revokeObjectURL(url);
}


let loadFile = document.getElementById('loadFile');

loadFile.addEventListener('change', function (e) {
    const reader = new FileReader();

    reader.onload = function() {
        loadTemplate(reader.result);
    };

    reader.readAsText(loadFile.files[0]);
});